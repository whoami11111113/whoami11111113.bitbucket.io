
var vm = new Vue({
    el: '#app',
    name: 'App',
    data:  {
        runs: {},
        heroes: {
            hero_1: {
                name: "Evangelo",
                color: {
                    hsl: "hsl(28.1, 90.2%, 48.2%)",
                    hex: "#f06d06"
                },
                image: 'Evangelo.png',
                selected: true
            },
            hero_2: {
                name: "Walker",
                color: {
                    hsl: "hsl(120, 100%, 25%)",
                    hex: "#008000"
                },
                image: 'Walker.png',
                selected: false
            },
            hero_3: {
                name: "Holly",
                color: {
                    hsl: "hsl(333, 90%, 59%)",
                    hex: "#F5388CC3"
                },
                image: 'Holly.png',
                selected: false
            },
            hero_4: {
                name: "Hoffman",
                color: {
                    hsl: "hsl(0, 89%, 57%)",
                    hex: "#F32F2FC3"
                },
                image: 'Hoffman.png',
                selected: false
            },
            hero_5: {
                name: "Doc",
                color: {
                    hsl: "hsl(219, 89%, 57%)",
                    hex: "#2F74F3D3"
                },
                image: 'Doc.png',
                selected: false
            },
            hero_6: {
                name: "Jim",
                color: {
                    hsl: "hsl(265, 76%, 51%)",
                    hex: "#7225E1D3"
                },
                image: 'Jim.png',
                selected: false
            },
            hero_7: {
                name: "Karlee",
                color: {
                    hsl: "hsl(57, 76%, 51%)",
                    hex: "#E1D825D3"
                },
                image: 'Karlee.png',
                selected: false
            },
            hero_8: {
                name: "Mom",
                color: {
                    hsl: "hsl(239, 86%, 49%)",
                    hex: "#1114EAD3"
                },
                image: 'Mom.png',
                selected: false
            }
        },
        maps: {

            // 1.1
            evans_a: {
                name: "Resurgence",
                act: "Act 1: The Devil's Return",
                img: "/maps/1_resurgence.png"
            },
            evans_b: {
                name: "Tunnel of Blood",
                act: "Act 1: The Devil's Return",
                img: "/maps/1_tunnel_of_blood.png"
            },
            evans_c: {
                name: "Pain Train",
                act: "Act 1: The Devil's Return",
                img: "/maps/1_pain_train.png"
            },
            evans_d: {
                name: "The Crossing",
                act: "Act 1: The Devil's Return",
                img: "/maps/1_the_crossing.png"
            },


            // 1.2
            finley_rescue_a: {
                name: "A Clean Sweep",
                act: "Act 1: Search and Rescue",
                img: "/maps/1_a_clean_sweep.png"
            },
            finley_rescue_b: {
                name: "Book Worms",
                act: "Act 1: Search and Rescue",
                img: "/maps/1_book_worms.png"
            },
            finley_rescue_c: {
                name: "Bar Room Blitz",
                act: "Act 1: Search and Rescue",
                img: "/maps/1_bar_room_blitz.png"
            },

            // 1.3
            finley_diner_a: {
                name: "Special Delivery",
                act: "Act 1: The Dark Before the Dawn",
                img: "/maps/1_special_delivery.png"
            },
            finley_diner_b: {
                name: "The Diner",
                act: "Act 1: The Dark Before the Dawn",
                img: "/maps/1_the_diner.png"
            },

            // 1.4
            bluedog_a: {
                name: "Bad Seeds",
                act: "Act 1: Blue Dog Hollow",
                img: "/maps/1_bad_seeds.png"
            },
            bluedog_b: {
                name: "Hell's Bells",
                act: "Act 1: Blue Dog Hollow",
                img: "/maps/1_hells_bells.png"
            },
            bluedog_c: {
                name: "Abandoned",
                act: "Act 1: Blue Dog Hollow",
                img: "/maps/1_abadoned.png"
            },
            bluedog_d: {
                name: "The Sound of Thunder",
                act: "Act 1: Blue Dog Hollow",
                img: "/maps/1_the_sound_of_thunder.png"
            },



            // 2.1
            finley_police_a: {
                name: "A Call to Arms",
                act: "Act 2: The Armory",
                img: "/maps/2_a_call_to_arms.png"
            },
            finley_police_b: {
                name: "The Handy Man",
                act: "Act 2: The Armory",
                img: "/maps/2_the_handy_man.png"
            },

            // 2.2
            clog_a: {
                name: "Pipe Cleaners",
                act: "Act 2: Plan B",
                img: "/maps/2_pipe_cleaners.png"
            },
            clog_b: {
                name: "Hinterland",
                act: "Act 2: Plan B",
                img: "/maps/2_hinterland.png"
            },
            clog_c: {
                name: "Trailer Trashed",
                act: "Act 2: Plan B",
                img: "/maps/2_trailer_trashed.png"
            },
            clog_d: {
                name: "The Clog",
                act: "Act 2: Plan B",
                img: "/maps/2_the_clog.png"
            },
            clog_e: {
                name: "The Broken Bird",
                act: "Act 2: Plan B",
                img: "/maps/2_the_broken_bird.png"
            },

            // 2.3
            finley_church_a: {
                name: "Heralds of the Worm Part 1",
                act: "Act 2: Job 10:22",
                img: "/maps/2_heralds_of_the_worm_part_1.png"
            },
            finley_church_b: {
                name: "Heralds of the Worm Part 2",
                act: "Act 2: Job 10:22",
                img: "/maps/2_heralds_of_the_worm_part_2.png"
            },
            finley_church_c: {
                name: "Grave Danger",
                act: "Act 2: Job 10:22",
                img: "/maps/2_grave_danger.png"
            },


            // 3.1
            manor_a: {
                name: "Farther Afield",
                act: "Act 3: Dr. Rogers Neighborhood",
                img: "/maps/3_farther_afield.png"
            },
            manor_b: {
                name: "Blazing Trails",
                act: "Act 3: Dr. Rogers Neighborhood",
                img: "/maps/3_blazing_trails.png"
            },
            manor_c: {
                name: "Cabins by the Lake",
                act: "Act 3: Dr. Rogers Neighborhood",
                img: "/maps/3_cabins_by_the_lake.png"
            },
            manor_d: {
                name: "Garden Party",
                act: "Act 3: Dr. Rogers Neighborhood",
                img: "/maps/3_garden_party.png"
            },
            manor_e: {
                name: "T5",
                act: "Act 3: Dr. Rogers Neighborhood",
                img: "/maps/3_t5.png"
            },

            // 3.2
            cdc_a: {
                name: "A Friend in Need",
                act: "Act 3: Remnants",
                img: "/maps/3_a_friend_in_need.png"
            },
            cdc_b: {
                name: "Making the Grade",
                act: "Act 3: Remnants",
                img: "/maps/3_making_the_grade.png"
            },
            cdc_c: {
                name: "The Road to Hell",
                act: "Act 3: Remnants",
                img: "/maps/3_the_road_to_hell.png"
            },
            cdc_d: {
                name: "The Body Dump",
                act: "Act 3: Remnants",
                img: "/maps/3_the_body_dump.png"
            },

            // 4
            titan_a: {
                name: "The Abomination ",
                act: "Act 4: The Abomination",
                img: "/maps/4_the_abomination.png"
            },
        }
    },
    computed: {
        hasData: function() {
            let res = Object.keys(this.runs).length > 0;
            if (res) {
                Vue.nextTick(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            }
            return res;
        },
        disabledDeselect() {
            let z = this.heroes;
            for (const [, val] of Object.entries(z)) {
                if (val.selected) {
                    return false;
                }
            }

            return true;
        },
        disabledSelect() {
            let z = this.heroes;
            for (const [, val] of Object.entries(z)) {
                if (!val.selected) {
                    return false;
                }
            }

            return true;
        },
    },
    methods: {
        selectAll() {
            for (const [index, val] of Object.entries(this.heroes)) {
                val.selected = true;
                this.$set(this.heroes, index, val)
            }
            this.$forceUpdate()
        },
        deselectAll() {
            for (const [index, val] of Object.entries(this.heroes)) {
                val.selected = false;
                this.$set(this.heroes, index, val)
            }
            this.$forceUpdate()
        },

        activeHero(hero) {
            return this.heroes[hero].selected;
        },

        toggleActive(hero) {
            let obj = this.heroes[hero]
            obj.selected = !this.heroes[hero].selected
            this.$set(this.heroes, hero, obj);
        },
        getColor(hero) {
          return this.heroes[hero].color.hex;
        },
        buildTooltip(map) {
            let users = [];
            for (const [hero, val] of Object.entries(this.heroes)) {
                if (Object.prototype.hasOwnProperty.call(this.runs, hero) && Object.values(this.runs[hero]).indexOf(map) > -1) {
                    users.push(this.heroes[hero].name);
                }
            }
            let showFinished = ''
            if (users.length > 0) {
                showFinished = "Finished by: <b>" + users.join(", ") + "</b>"
            }
            let showMap = this.maps[map].act + ", " + this.maps[map].name;
            return `<div>
                <div>
                <img src="${this.maps[map].img}" width="300">
                </div>
                <div>${showMap}</div>
                <div>${showFinished}</div>
                </div>`
        },
        buildStyle(map) {

            let style = `box-shadow:`;
            let added = 0;
            for (const [hero, val] of Object.entries(this.heroes)) {
                if (val.selected && Object.prototype.hasOwnProperty.call(this.runs, hero) && Object.values(this.runs[hero]).indexOf(map) > -1) {
                    style = style + (added > 0 ? ',' : '') + " 0 0 0 5px #c4c4c4";
                    added++
                    break;
                }
            }
            if (added === 0) {
                return "";
            }
            return style + ";";
        },
        parseFile(ev) {

            const file = ev.target.files[0];
            const reader = new FileReader();

            if (!ev.target.files[0].name.includes(".sav")) {
                alert("File extension must be .sav");
            }

            reader.onload = function(e) {
                let json = JSON.parse(e.target.result);
                let regex = /(\w+)::hard::(\w+)/;
                for (const [key] of Object.entries(json['onlineDataCache']['stats']['missionsCompleted_Secured']['keys'])) {
                    let matches = key.match(regex)
                    if (matches) {
                        let player = matches[2];
                        let map = matches[1];
                        if (!Object.prototype.hasOwnProperty.call(vm.runs, player)) {
                            vm.$set(vm.runs, player, [])
                        }
                        vm.runs[player].push(map)
                    }
                }
                if (Object.keys(vm.runs).length === 0) {
                    alert("Something wrong with your file, nothing loaded.")
                }
            }
            reader.readAsText(file);
        },
    }})
